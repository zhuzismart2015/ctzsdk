//
//  main.m
//  MyLib
//
//  Created by admin on 10/07/2016.
//  Copyright (c) 2016 admin. All rights reserved.
//

@import UIKit;
#import "CTZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CTZAppDelegate class]));
    }
}
