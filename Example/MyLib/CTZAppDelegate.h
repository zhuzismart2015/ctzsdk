//
//  CTZAppDelegate.h
//  MyLib
//
//  Created by admin on 10/07/2016.
//  Copyright (c) 2016 admin. All rights reserved.
//

@import UIKit;

@interface CTZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
